# beginning of main3.py
from flask import render_template
#from models import  app, db, Book
from create_db import app, db, Book, create_books


#books = [{'title': 'Software Engineering', 'id': '1'}, {'title':'Algorithm Design', 'id':'2'},{'title':'Python', 'id':'3'}]

@app.route('/')
def index():
	return render_template('hello.html')
	
@app.route('/book2/')
def book():
	books = db.session.query(Book).all()
	return render_template('book2.html', books = books)
	
if __name__ == "__main__":
	app.run()
# end of main3.py
